<h1>Open Registers</h1>
<pre class="metadata">
Logo: https://assets.digital.cabinet-office.gov.uk/static/images/gov.uk_logotype_crown.png
Status: ED
ED: https://openregister.github.io/specification/
Shortname: openregister-core
Level: 1
Editor: Paul Downey, Government Digital Service, paul.downey@digital.cabinet-office.gov.uk
Editor: Daniel Appelquist, Government Digital Service, daniel.appelquist@digital.cabinet-office.gov.uk
Group: openregister
Indent: 2
Abstract:
  This document defines the resources and representations which together
  provide an Application Programming Interface (API) for accessing 
  data held in an open register.

Version History: https://github.com/openregister/specification/commits/gh-pages/index.bs
!Bug Reports: <a href="https://github.com/openregister/specification/issues/new">via the openregister/specification repository on GitHub</a>
</pre>

<pre class=include>
path: include/logo-openregister.include
</pre>

<pre class=include>
path: include/copyright-openregister.include
</pre>

<style>
  svg.railroad-diagram { width: 100%; }

  dt {
    font-weight: bold;
    font-style: italic;
    color: #005A9C;
  }

  dl.resource {
    background: #DEF;
    border-left: 0.5em solid #8CCBF2;
  }
  dl.resource dt {
    display: inline-block;
    min-width: 100px;
    padding: 0.5em;
    font-style: normal;
    font-weight: normal;
    text-transform: uppercase;
  }
  dl.resource dd {
    display: inline;
    margin: 0;
  }
  dl.resource dd:after{
    display: block;
    content: '';
  }
  dl.resource a {
    color: #005A9C;
    padding: 0 0.25em;
  }
</style>

# Introduction # {#introduction}

<em>An introduction to the product, independent of HMG's use of registers ..</em>

# Infoset # {#infoset}

Note: Data items in this specification are defined in terms of an information set which can be mapped to one of a number of different representations. There is no canonical representation.

* An infoset is an unordered collection of data items.
* A data item is identified within the scope of the infoset by a [[#field-field]] value.
* The contents of a data item is constrained by one of a number of different [[#datatypes]].


# Entity resources # {#entity-resources}

## Item resource ## {#item-resource}

<dl class="resource"><dt>Path</dt><dd>/item/{<a href="#item-hash-field">item-hash</a>}</dd></dt></dl>

An item is an unordered collection of [[#fields]] and values.
The set of fields which MAY be included in an item are defined in the 
[[#fields-field]] field in the [[#register-register]] entry for the register.

An item is identified by the globally unique [[#item-hash-field]]
calculated from its contents. Changing the item data changes the [[#item-hash-field]].

<div class="example">
The following example shows an item in the [[#json-representation]]:
<pre highlight="json">
{
  "business": "company:07228130",
  "food-premises": "788112",
  "food-premises-types": ["restaurant", "cafe"],
  "local-authority": "E09000015",
  "name": "Roy's Rolls",
  "premises": "13456079000",
  "start-date": "2015-03-01"
}
</pre>
</div>

ISSUE: IMPLEMENTATION the path to an item is currently implemented as /hash/{item-hash} and returns a record, not an item.

## Entry resource ## {#entry-resource}

<dl class="resource"><dt>Path</dt><dd>/entry/{<a href="#entry-number-field">entry-number</a>}</dd></dt></dl>

<div class="example">
The following example shows an entry in the [[#json-representation]]:

<pre highlight="json">
{
    "entry-number": "72",
    "item-hash": "d9178efd8febfebaaa42968648b7bdd023369c7f",
    "timestamp": "2015-08-20T08:15:30Z"
}
</pre>
</div>

ISSUE: IMPLEMENTATION entry timestamps are currently being recorded in localtime with a "-05:00" suffix. We should force them to be UTC to match the [[#timestamp-datatype]].

## Record resource ## {#record-resource}

<dl class="resource"><dt>Path</dt><dd>/record/{<a href="#field-value">field-value</a>}</dd></dt></dl>


<div class="example">
The following example shows a record in the [[#json-representation]]:

<pre highlight="json">
{
    "entry": {
      "entry-number": "72",
      "item-hash": "d9178efd8febfebaaa42968648b7bdd023369c7f",
      "timestamp": "2015-08-20T08:15:30Z"
    },
    "item": {
      "local-authority": "E09000019",
      "name": "Islington"
    }
}
</pre>
</div>


## Register resource ## {#register-resource}

<dl class="resource"><dt>Path</dt><dd>/register</dd></dt></dl>

The register resource is [[#infoset]] with the following fields:

  : [[#domain-field]]
  :: The Internet domain the register is available from.
  : [[#last-updated-field]]
  :: The date the register was last updated.

The register resource also contains the following data items:

  : register-record
  :: A copy of the [[#register-register]] [[#record-resource]] entity describing this register.
  : total-items
  :: An [[#integer-datatype]] value representing the number of [[#item-resource]] entities currently stored in the register.
  : total-entries
  :: An [[#integer-datatype]] value representing the number of [[#entry-resource]] entities currently stored in the register.
  : total-records
  :: An [[#integer-datatype]] value representing the number of [[#record-resource]] entities currently stored in the register.


<div class="example">
The following example shows a register in the [[#json-representation]]:

<pre highlight="json">
{
  "domain": ".register.gov.uk",
  "last-updated": "2016-01-21T21:09:59Z",
  "register-record": {
    "entry": {
      "entry-number": "12",
      "item-hash": "d9178efd8febfebaaa42968648b7bdd023369c7f",
      "timestamp": "2015-08-20T08:15:30Z"
    },
    "item": {
      "fields": [ "address", "end-date", "school", "start-date", "name", "website" ],
      "name": "Schools in the UK",
      "phase": "beta",
      "register": "school",
      "registry": "department-of-education",
      "start-date": "2012-01-01"
    }
  }
  "total-entries": "109001",
  "total-items": "109009",
  "total-records": "30522"
}
</pre>
</div>

## Proof resource ## {#proof-resource}
<dl class="resource"><dt>Path</dt><dd>/proof/{<a href="#proof-identifier-field">proof-identifier</a>}</dd></dt></dl>

<div class="example">
The following example shows a register proof in the [[#json-representation]]:

<pre>
https://school.register.gov.uk/proof/certificate-transparency
</pre>
<pre highlight="json">
{
  "tree-size": "9803348",
  "timestamp": "1447421303202",
  "sha256-root-hash": "JATHxRF5gczvNPP1S1WuhD8jSx2bl+WoTt8bIE3YKvU=",
  "tree-head-signature":
  "BAMARzBFAiEAkKM3aRUBKhShdCyrGLdd8lYBV52FLrwqjHa5/YuzK7ECIFTlRmNuKLqbVQv0QS8nq0pAUwgbilKOR5piBAIC8LpS"
}
</pre>
</div>

ISSUE: CT timestamp is inconsistent with [[#timestamp-datatype]].

ISSUE: CT names should be mapped to a consistent "proof" field names, or held inside an envelope.

## Proof node resource ## {#proof-node-resource}
<dl class="resource"><dt>Path</dt><dd>/proof/{<a href="#proof-identifier-field">proof-identifier</a>}/node/{<a href="#proof-node-hash-field">proof-node-hash</a>}</dd></dt></dl>

<div class="example">
The following example shows a register proof in the [[#json-representation]]:

<pre highlight="json">
{
  "proof": "certificate-transparency",
  "proof-node-hashes": [
    "zeC0N4isU4ZrwaeCJdjy31+X9avF8zt87NN6G+xOThQ=",
    "WBb5QnpttrokZ3kMCNNyAymfQK/BfPgIdsh3izVZgK0=",
    "lyzm0rW0RIi1OXHwnpQ4WVB+cbSzLplTeAWrIDZaavU="
  ]
}
</pre>
</div>

# Immutable resources # {#immutable-resources}

An immutable resource, is one whose contents will never change.
An instance of an [[#item-resource]], [[#entry-resource]] and [[#proof-node-resource]] are all deemed to be immutable.

# Collection resources # {#list-resources}

There is a limit to how many records or entries can be returned in a single request. To fetch more, pagination is used. Pagination is supported through an http link header (RFC 5988), with a link rel="next" for the next page, and rel="previous" for the previous page. On the first page, there is no "previous" link, and on the last page there is no "next" link.

ISSUE: define query string parameters ..

ISSUE: if /records and /items are sets, what does that mean for pagination?

## Items resource ## {#items-resource}

<dl class="resource"><dt>Path</dt><dd>/items</dd></dt></dl>

ISSUE: IMPLEMENTATION /items isn't yet implemented. There may not be a need for this resource as it's available in the archive.

<div class="example">
The following example shows a set of records in the [[#json-representation]]:

<pre highlight="json">
{
  "1a0212ba5094383bcc2a0bbe1a55e3a1f1278984": {
    "local-authority": "E09000019",
    "name": "Islington"
  },
  "d9178efd8febfebaaa42968648b7bdd023369c7f": {
    "local-authority": "E09000016",
    "name": "Havering"
  }
}
</pre>
</div>


## Entries resource ## {#entries-resource}

<dl class="resource"><dt>Path</dt><dd>/entries</dd></dt></dl>

ISSUE: IMPLEMENTATION /entries is currently implemented as also returning the item contents.

<div class="example">
The following example shows a list of entries in the [[#json-representation]]:

<pre highlight="json">
[
  {
    "entry-number": "72",
    "item-hash": "13f6de75b9f6d970691985e72a7dfa211bad1591",
    "timestamp": "2015-08-20T08:15:30Z"
  },
  {
    "entry-number": "21",
    "item-hash": "1a0212ba5094383bcc2a0bbe1a55e3a1f1278984",
    "timestamp": "2015-08-15T08:15:30Z"
  }
]
</pre>
</div>


## Item entries resource ## {#item-entries-resource}

<dl class="resource"><dt>Path</dt><dd>/item/{<a href="#item-hash-field">item-hash</a>}/entries</dd></dt></dl>

An ordered list of [[#entry-resource]] values which cite the item.

ISSUE: IMPLEMENTATION The resource is /item/{item-hash}/entries is new.

<div class="example">
The following example shows a list of item entries in the [[#json-representation]]:

<pre highlight="json">
[
  {
    "entry-number": "121",
    "item-hash": "c8844f3961a9a90812b8992ad8dbd5495e0f4782",
    "timestamp": "2015-08-20T08:15:30Z"
  },
  {
    "entry-number": "133",
    "item-hash": "13f6de75b9f6d970691985e72a7dfa211bad1591",
    "timestamp": "2015-08-15T08:15:30Z"
  }
]
</pre>
</div>


## Record entries resource ## {#record-entries-resource}

<dl class="resource"><dt>Path</dt><dd>/record/{<a href="#field-value">field-value</a>}/entries</dd></dt></dl>

ISSUE: IMPLEMENTATION the path /{key-field-name}/{field-value}/history is replaced by {record}/entries and {item}/entries

<div class="example">
The following example shows a list of item entries in the [[#json-representation]]:

<pre highlight="json">
[
  {
    "entry-number": "121",
    "item-hash": "c8844f3961a9a90812b8992ad8dbd5495e0f4782",
    "timestamp": "2015-08-20T08:15:30Z"
  },
  {
    "entry-number": "133",
    "item-hash": "ffb781149473e7244109e45165260702fe39cede",
    "timestamp": "2015-08-15T08:15:30Z"
  }
]
</pre>
</div>


## Records resource ## {#records-resource}

<dl class="resource"><dt>Path</dt><dd>/records</dd></dt></dl>

ISSUE: IMPLEMENTATION this is a set, so have changed it from a list to a hash with the record id as the key.

<div class="example">
The following example shows a list of records in the [[#json-representation]]:

<pre highlight="json">
{
  "E09000019": {
    "entry": {
      "entry-number": "72",
      "item-hash": "06ba6a095dc6244cf742bf0bac95b7d8d519c9d9",
      "timestamp": "2015-08-20T08:15:30Z"
    },
    "item": {
      "local-authority": "E09000019",
      "name": "Islington"
    }
  },
  "E09000016": {
    "entry": {
      "entry-number": "76",
      "item-hash": "0ac85d452fdfd10414a7d113f990a617b590633f",
      "timestamp": "2015-08-20T08:15:30Z"
    },
    "item": {
      "local-authority": "E09000016",
      "name": "Havering"
    }
  }
}
</pre>
</div>

## Faceted records resource ## {#faceted-records-resource}

<dl class="resource"><dt>Path</dt><dd>/records/{<a href="#field-field"">field-name</a>}/{<a href="#field-value"">field-value</a>}</dd></dt></dl>

<div class="example">
The following example shows a list of record entries matching a field in the [[#json-representation]]:

<pre highlight="http">
http://school.register.gov.uk/religious-character/Quaker.json
</pre>
<pre highlight="json">
{
  "123278": {
    "entry": {
      "entry-number": "18371",
      "item-hash": "06ba6a095dc6244cf742bf0bac95b7d8d519c9d9",
      "timestamp": "2016-01-01T00:00:00Z"
    },
    "item": {
      "address": "10011891998",
      "maximum-age": "18",
      "minimum-age": "2",
      "name": "Sibford School",
      "religious-character": "Quaker",
      "school": "123278",
      "start-date": "1945-01-01",
      "website": "http://www.sibford.oxon.sch.uk"
    }
  },
  "121728": {
    "entry": {
      "entry-number": "17164",
      "item-hash": "0ac85d452fdfd10414a7d113f990a617b590633f",
      "timestamp": "2016-01-01T00:00:00Z"
    },
    "item": {
      "address": "200004778207",
      "end-date": "2006-02-28",
      "headteacher": "Mrs S Ratcliffe",
      "name": "Boothan Junior School",
      "religious-character": "Quaker",
      "school": "121728",
      "start-date": "1957-10-21",
      "website": "http://www.bootham.york.sch.uk/ebor"
    }
  }
}
</pre>
</div>

## Proofs resource ## {#proofs-resource}

<dl class="resource"><dt>Path</dt><dd>/proofs</dd></dt></dl>

<div class="example">
The following example shows a set of register proofs in the [[#json-representation]]:

<pre highlight="json">
['certificate-transparency']
</pre>
</div>

Note: A register MAY have more than one proof, to support multiple types of proof in the future.

## Entry proof nodes resource ## {#entry-proof-resource}

<dl class="resource"><dt>Path</dt><dd>/entry/{<a href="#entry-number-field">entry-number</a>}/proofs</dd></dt></dl>

A set of links to the [[#proof-resource]]s and [[#proof-node-resource]]s which reference this entry.


# Archive resources # {#archive-resources}

## Download page ## {#download}

<dl class="resource"><dt>Path</dt><dd>/download</dd></dt></dl>

The contents of an open register MUST be made available as an archive.
The archive file MUST be capable of being used as backup of the register, with the exception of secrets used to generate [[#digital-proofs]].
The archive file SHOULD be made available in a single file, but MAY be split into multiple parts if it deemed too large.

The archive contains the following files in the following structure:

 * a directory with the name of the register containing
   * a file named "register.json" containing the [[#register-resource]] in the [[#json-representation]]
   * a file named "proof.json" containing one or more digital proofs for the register in the [[#json-representation]]
   * a directory named "item" containing all of the [[#item-resource]] in one or more parts in the [[#json-representation]]
   * a directory named "entry" containing all of the [[#entry-resource]] in one or more files in the [[#json-representation]]

A register archive MAY contain entry and item resources in the more space efficient [[#tsv-representation]].

ISSUE: Do we provide downloads in tar.gz or [[ZIP]] (the ISO/IEC 21320-1:2015 profile is open)?

ISSUE: What is the naming convention for the archive files themselves?

ISSUE: How about a "record" archive containing only the latest entries for each record?


# Streaming resources # {#streaming-resources}

ISSUE: we don't yet know how to support updating an index or a cache, beyond polling. Maybe [[EVENTSOURCE]]?



# HTTP Headers # {#http-headers}

Table of link and other HTTP headers used by resources ..

  * [[RFC5988]]
  * <a href="http://www.iana.org/assignments/link-relations/link-relations.xhtml">link-relations</a>
  * [[#immutable-resources]] SHOULD be served with a long-lived Cache-Control max-age value [[RFC7234]].
  * the [[#item-hash-field]] SHOULD be served as the etag header value for an [[#item-resource]].


<div class="example">
The following example shows the HTTP headers for the [[#json-representation]] of an immutable [[#item-resource]]:
<pre>
HTTP/1.1 200 OK
Date: Fri, 22 Jan 2016 08:00:08 GMT
Expires: Sun, 22 Jan 2017 08:00:08 GMT
Link: &lt;/school/12345/entries>; rel="version-history"
Content-Type: application/json
Content-Security-Policy: default-src 'self'
Cache-Control: no-transform, max-age=31536000
etag: c2f6fb7ed8332561f2252359b7d6f173a376a942
X-Xss-Protection "1; mode=block"
X-Frame-Options "SAMEORIGIN"
X-Content-Type-Options "nosniff"
Vary: Accept-Encoding
Content-Length: 522
</pre>
</div>

ISSUE: should provide more Content-Security-Policy values such as "https:", "data:" "unsafe-inline" and "unsafe-eval"?

ISSUE: should we specify <a href="https://scotthelme.co.uk/hpkp-http-public-key-pinning/">HTTP Public Key Pinning (HPKP)</a>?

# Datatypes # {#datatypes}

## String datatype ## {#string-datatype}
  * [[UNICODE]] [[UTF-8]]

## Field-name datatype ## {#fieldname-datatype}

## Integer datatype ## {#integer-datatype}

<pre class='railroad'>
Choice:
  T: 0
  Sequence:
    Optional:
      T: -
    N: digit 1 to 9
    ZeroOrMore:
      N: digit 0 to 9
</pre>
All values are decimal. Leading zeros are not allowed, except for the integer '0', which is represented as the string “0”.
Negative values are marked with a leading “-” character ([[UNICODE]] 0x2D HYPHEN-MINUS).

<div class="example">
  The following examples are all valid integer values: 
  <pre>
    "100", "0", "-200"
  </pre>
</div>

## Datetime datatype ## {#datetime-datatype}

<pre class='railroad'>
T: YYYY
Optional:
  Sequence:
    T: -
    T: MM
    Optional:
      Sequence:
        T: -
        T: DD
        Optional:
          Sequence:
            T: T
            T: HH
            Optional:
              Sequence:
                T: :
                T: MM
                Optional:
                  Sequence:
                    T: :
                    T: SS
</pre>



  * Datetime values MUST be recorded as Universal Coordinated Time (UTC), and not local time such as British Summer Time (BST) or other offset from UTC.
  * Datetime values are all valid [[ISO8601]].
  * The may be taken as a consumer as an indication of the precision, in which case it is the responsibility of the consumer to decide how the date should be interpreted.

<div class="example">
  The following examples are all valid Datetime values: 
  <pre>
    "2001", "2001-01", "2001-01-31", "2001-01-31T23:20:55"
  </pre>
</div>

## Timestamp datatype ## {#timestamp-datatype}
<pre class='railroad'>
Sequence:
  T: YYYY
  T: -
  T: MM
  T: -
  T: DD
  T: T
  T: HH
  T: :
  T: MM
  T: :
  T: SS
  T: Z
</pre>

  * [[RFC3339]] timestamp, MUST be in UTC.

<div class="example">
  The following example is a valid Timestamp value: 
  <pre>
    "2001-01-31T23:20:55Z"
  </pre>
</div>

## Point datatype ## {#point-datatype}
  * point [[GEOJSON]]

## Multipolygon datatype ## {#multipolygon-datatype}
  * multipolygon [[GEOJSON]]

## Text datatype ## {#text-datatype}
  * text [[MARKDOWN]]

## CURIE datatype ## {#curie-datatype}
Used to identify a record, possibly in another register.
  * curie [[CURIE]]

## Item-hash datatype ## {#item-hash-datatype}
The object hash of the item's contents may be used to identify the contents of an item, irrespective of where it is stored or presented.

ISSUE: do we need to include the alogrithm in the hash, eg "sha512:aa3344.." ?

## Entry-reference datatype ## {#entry-reference-datatype}
Legal documents may need to cite an individual entry, rather than the latest entry of the record.

ISSUE: we don't know how to globally reference an entry, by its entry-number "register[33]", the tuple of entry-number, item-hash tuple or its fingerprint from the digital proof.

## URL datatype ## {#url-datatype}
Used to link to an external website, or other resource not held in a register.
  * URL [[URI]]


# Fields # {#fields}

ISSUE: We use semicolons to separate fields in a TSV/CSV value. Is this the best delimiter to use?

<em>Fields defined by this specification</em>

## cardinality ## {#cardinality-field}

## domain ## {#domain-field}

## entry-number ## {#entry-number-field}

ISSUE: IMPLEMENTATION serial-number is inconsistent and has therefore been renamed as entry-number

## item-hash ## {#item-hash-field}

ISSUE: objecthash isn't a specification, we'll need to define our algorithm here

ISSUE: we need to ensure our objecthash generates the same value for an empty and a missing field.

  * <a href="https://github.com/benlaurie/objecthash">objecthash</a>
  * [[FIPS-180-4]]

## last-updated ## {#last-updated-field}

## proof-identifier ## {#proof-identifier-field}

## proof-node-hash ## {#proof-node-hash-field}

## field ## {#field-field}


# Registers # {#registers}

## Register register ## {#register-register}

## Field register ## {#field-register}

## Datatype register ## {#datatype-register}




# Representations # {#representations}

Note: JSON and other representations can have a field which is missing. These have the same semantics as an empty field.

ISSUE: IMPLEMENTATION currently presentation is emitting names without double-quotes, which according to <a href="http://jsonlint.com/">jsonlint.com</a> is not valid JSON.

## HTML representation ## {#html-representation}
  * [[HTML5]]

ISSUE: do we specify RDFa or Schema.org markup?

## JSON representation ## {#json-representation}
  * suffix: .json
  * media-type: application/json
  * specification: [[JSON]] 

  All field values MUST be encoded as JSON strings.

ISSUE: the JSON should be in a c14n format, but no spec exists, so we will need to define what that means?

## YAML representation ## {#yaml-representation}

  * Suffix: .yaml
  * Content-Type: text/yaml;charset=UTF-8
  * Specification: [[YAML]] 

<div class="example">
The following example shows a [[#record-resource]] in the [[#yaml-representation]]:
<pre highlight="yaml">
entry:
  entry-number: "30568"
  timestamp: "2015-01-02T23:59:01Z"
  item-hash: "87963123bd04263c878b36ad7ce421b8b68a07f9"
item:
  address: "100101030506"
  name: "Glanaman Home Tution Centre"
  school: "402175"
  start-date: "2007-11-07"
</pre>
</div>

## CSV representation ## {#csv-representation}
  * Suffix: .csv
  * Specification: [[tabular-data-model]]

## TSV representation ## {#tsv-representation}
  * Suffix: .tsv
  * Content-Type: text/tab-separated-values;charset=UTF-8
  * Specification: [[IANA-TSV]]


</pre>
</div>

## JSON-LD representation ## {#json-ld-representation}
  * [[JSON-LD]]

## Turtle representation ## {#ttl-representation}
  * Suffix: .ttl
  * Content-Type: text/turtle;charset=UTF-8
  * Specification: [[TURTLE]]

<div class="example">
The following example shows an [[#item-resource]] in the [[#ttl-representation]]:
<pre>
@prefix field: &lt;http://field.register.gov.uk/field/&gt;.

&lt;http://school.register.gov.uk/item/af3056bd04263c878b36ad7ce421b8b68a0799&gt;
 field:address &lt;http://address.register.gov.uk/address/100101030506&gt; ;
 field:name "Glanaman Home Tution Centre" ;
 field:school &lt;http://school.register.gov.uk/record/402175&gt; ;
 field:start-date "2007-11-07" ;
</pre>
</div>

## Atom representation ## {#atom-representation}
  * [[RFC4287]]






# Digital Proofs # {#digital-proofs}

## Certificate transparency ## {#using-certificate-transparency}
  Certificate Transparency [[RFC6962]] is one of a number of possible methods of proving the integrity of a register.

### Signed tree head ### {#signed-tree-head}

ISSUE: What happens when a key is rotated?

### Leaf signature ### {#leaf-signature}

### Verifying the register ### {#ct-verify-register}

### Verifying an entry ### {#ct-verify-entry}



# Minting a new entry # {#minting}

  To mint a new entry in the register:
  * POST a new item in [[#json-representation]] to the ..mint url.. with the proposed new entry number.
  * The item will first appear as a new item in the store
  * <em> .. status codes and error cases </em>
  * <em> .. 202 Accepted with a Location header of the [[#entry-resource]] for the new entry
  * The proposer can only trust it has been secured when the entry
    is listed in the [[#entries-resource]] with the same [[#entry-number-field]],
    and the [[#entry-resource]] is a covered by one or more [[#digital-proofs]] for the register.

The item MUST NOT contain empty fields.

ISSUE: does a register have a standard URL to POST a new entry?


# Redaction # {#redaction}

* An item may be removed from a register, but the entry MUST NOT be removed from the register.

ISSUE: there is a need redaction, even for open records such as insolvency and bankruptcy.

ISSUE: need a mechanism to flag an item as redacted.

ISSUE: need a mechanism to mark a proof as deprecated.

ISSUE: need a mechanism to terminate a proof and start a new proof.



# Versioning and extensibility # {#versioning-and-extensibility}

## Backwards compitability ## {#backwards-compatibility}
  * Semantics of a field cannot be significantly changed but not changed.

## Forwards compatibility ## {#forwards-compatibility}
  * Must-ignore rule.
  * Defaulting empty or missing values


## Points of extensibility ## {#extension-points}

### Fields ### {#extensibility-fields}
### Resources ### {#extensibility-resources}
### Proofs ### {#extensibility-proofs}
### Representations ### {#extensibility-representations}

A register may provide additional, possibly domain specific representations.

<div class="example">
A register containing fields with [[#point-datatype]] or [[#multipolygon-datatype]] values may also serve a list of items as [[GML]], [[KML]] or other geographical representation.
</div>

Additional representations for resources SHOULD be linked to from the HTML representation of the resource.

# Security considerations # {#security-considerations}

## Mint access control ## {#mint-access-control}
## Compromised proof ## {#compromised-proof}
## Stale record ## {#stale-record}
## Denial of service ## {#denial-of-service}
## DNS ## {#DNS}
## Protecting the private key ## {#private-key}
## Compromised private key ## {#compromised-private-key}
## Hash clash ## {#hash-clash}


<pre class="link-defaults">
spec:html5; type:element; text:script
</pre>

<pre class="biblio">
{
  "RFC6962": {
      "href": "https://tools.ietf.org/html/rfc6962",
      "title": "Certificate Transparency",
      "publisher": "IETF",
      "authors": [ "B. Laurie", "A. Langley", "E. Kasper" ]
  },
  "GEOJSON": {
      "href": "https://tools.ietf.org/html/draft-ietf-geojson-00",
      "title": "The GeoJSON Format",
      "publisher": "IETF",
      "authors": [ "H. Butler", "M. Daly", "A. Doyle", "S. Gillies", "T. Schaub", "S. Hagen" ]
  },
  "MARKDOWN": {
      "href": "http://spec.commonmark.org/0.24/",
      "title": "Common Markdown",
      "publisher": "CommonMark",
      "authors": [ "John MacFarlane", "David Greenspan", "Vicent Marti", "Neil Williams", "Benjamin Dumke-von der Ehe", "Jeff Atwood" ]
  },
  "KML": {
      "href": "http://www.opengeospatial.org/standards/kml/",
      "title": "KML 2.3",
      "publisher": "OGC"
  }

}
</pre>

# Namespaces # {#namespaces}

* defined by this specification
* defined by {#core-registers}
